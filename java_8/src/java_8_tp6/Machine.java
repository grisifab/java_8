package java_8_tp6;

public class Machine {
	
	private int annee;
	private String nom;
	
	public int getAnnee() {
		return annee;
	}
	public void setAnnee(int annee) {
		this.annee = annee;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	@Override
	public String toString() {
		return "Machine [annee=" + annee + ", nom=" + nom + "]";
	}
	public Machine(int annee, String nom) {
		super();
		this.annee = annee;
		this.nom = nom;
	}
	
	

}
