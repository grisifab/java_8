package java_8_tp6;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class Dummy {
	
	public static void main(String[] args) {

		Thread th1 = new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println(Thread.currentThread().getName());
			}
		});
		th1.start();
		
		
		// M�thode Lambda
		Thread th2 = new Thread(() -> {
			System.out.println("Thread runnable " + Thread.currentThread().getName());
		});
		th2.start();
		
		// partie 2 --------------------------------------
		
		
		List<Machine> machines = new ArrayList<Machine>();
		machines.add(new Machine(2004, "ponceuse"));
		machines.add(new Machine(2005, "defonceuse"));
		machines.add(new Machine(2000, "tour"));
		machines.add(new Machine(2001, "perceuse"));
		machines.add(new Machine(2002, "fraiseuse"));
		machines.add(new Machine(2003, "meuleuse"));
		machines.forEach(System.out::println);
		
		Comparator<Machine> compMach = new Comparator <Machine>() {		
			@Override
			public int compare(Machine o1, Machine o2) {
				// TODO Auto-generated method stub
				return o1.getAnnee()-o2.getAnnee();
			}	};	
			
		Collections.sort(machines,compMach);
		machines.forEach(System.out::println);
		
		Comparator<Machine> compMach2 = (Machine o1, Machine o2) -> o2.getAnnee()-o1.getAnnee();
		
		Collections.sort(machines,compMach2);
		machines.forEach(System.out::println);
	}

}
