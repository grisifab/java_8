package java_8_tp10;

import java.util.function.Consumer;

import java_8_tp7.ImethodReference;

public class Dummy {

	public void chanter() {
		System.out.println("Je chaaaaaaaaaaaaaante !!!");
	}

	public static void testerThisDansMethodeStatique() {
		Consumer<String> c = (e) -> {
			// this.chanter(); //this n'existe pas dans un contexte statique
			System.out.println(e);
		};
		c.accept("Enfin la pluie !!!");
	}

	// Méthode d'instance
	public void marcher() {

		Consumer<String> c = (e) -> {
			this.chanter();
			System.out.println(e);
		};
		c.accept("Enfin la pluie !!!");
	}

	public void maMethode() {
		ImethodReference imr2 = ()-> {System.out.println("new message");};
	}
	
	// M�thode d'instance
	public void marcherAvecClasseAnonyme() {

		// Astuce pour utiliser l'objet Dummy dans la classe anonyme
		final Dummy that = this;
		Consumer<String> c = new Consumer<String>() {
			@Override
			public void accept(String e) {
				// this.chanter(); //this ici c'est celui de Consumer donc ne fonctionne pas
				that.chanter();
				System.out.println(e);
			}
		};
		c.accept("Enfin la pluie !!!");
	}

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Dummy d = new Dummy();
		d.marcher();
		int valeur = 1;
	}

}
