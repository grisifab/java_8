package java_8_tp11;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.*;

public class Gagnants {

	private int annnee;
	private String nationalite;
	private String nom;
	private String equipe;
	private int kilometres;
	private Duration temps;
	private int joursenjaune;

	public Gagnants(int annee, String nationalite, String nom, String equipe, int kilometres, Duration temps,
			int joursenjaune) {
		this.annnee = annee;
		this.nationalite = nationalite;
		this.nom = nom;
		this.equipe = equipe;
		this.kilometres = kilometres;
		this.temps = temps;
		this.joursenjaune = joursenjaune;
	}

	public static final List<Gagnants> gagnantsTDF = Arrays.asList(
			new Gagnants(2006, "Spain", "�scar Pereiro", "Caisse d'Epargne�Illes Balears", 3657,
					Duration.parse("PT89H40M27S"), 8),
			new Gagnants(2007, "Spain", "Alberto Contador", "Discovery Channel", 3570, Duration.parse("PT91H00M26S"),
					4),
			new Gagnants(2008, "Spain", "Carlos Sastre", "Team CSC", 3559, Duration.parse("PT87H52M52S"), 5),
			new Gagnants(2009, "Spain", "Alberto Contador", "Astana", 3459, Duration.parse("PT85H48M35S"), 7),
			new Gagnants(2010, "Luxembourg", "Andy Schleck", "Team Saxo Bank", 3642, Duration.parse("PT91H59M27S"), 12),
			new Gagnants(2011, "Australia", "Cadel Evans", "BMC Racing Team", 3430, Duration.parse("PT86H12M22S"), 2),
			new Gagnants(2012, "Great Britain", "Bradley Wiggins", "Team Sky", 3496, Duration.parse("PT87H34M47S"), 14),
			new Gagnants(2013, "Great Britain", "Chris Froome", "Team Sky", 3404, Duration.parse("PT83H56M20S"), 14),
			new Gagnants(2014, "Italy", "Vincenzo Nibali", "Astana", 3661, Duration.parse("PT89H59M06S"), 19),
			new Gagnants(2015, "Great Britain", "Chris Froome", "Team Sky", 3360, Duration.parse("PT84H46M14S"), 16),
			new Gagnants(2016, "Great Britain", "Chris Froome", "Team Sky", 3529, Duration.parse("PT89H04M48S"), 14));

	public static void main(String args[]) {
		
		//exo1
		//Cr�er une liste des noms des gagnants ayant parcouru moins de 3500km. 
		//Vous utiliserez : stream, filter, les r�f�rences de m�thode avec ::, et enfin collect. 	 
		List<Gagnants> lg1 = new ArrayList<Gagnants>();
		Stream<Gagnants>sg1 = gagnantsTDF.stream();
		lg1 = sg1.filter(x->x.getKilometres()<3500).collect(Collectors.toList());
		System.out.println(lg1);
		sg1.close();
		
		//exo2 M�me exercice, mais avec ceux ayant parcouru 3500km et plus. 
		List<Gagnants> lg2 = new ArrayList<Gagnants>();
		Stream<Gagnants>sg2 = gagnantsTDF.stream();
		lg2 = sg2.filter(x->x.getKilometres()>=3500).collect(Collectors.toList());
		System.out.println(lg2);
		sg1.close();
		
		//exo3M�me exercice que l�exercice 1, mais en ne pr�sentant que les deux premiers. Vous utiliserez en plus limit(2). 
		List<Gagnants> lg3 = new ArrayList<Gagnants>();
		Stream<Gagnants> sg3 = gagnantsTDF.stream();
		lg3 = sg3.filter(x -> x.getKilometres() < 3500).limit(2).collect(Collectors.toList());
		System.out.println(lg3);
		sg3.close();
		
		//exo4 Listez les noms distincts de tous les gagnants, quel que soit le nombre de km parcourus. Vous utiliserez distinct. 
		Stream<Gagnants> sg4 = gagnantsTDF.stream();
		sg4.map(g -> g.getNom()).distinct().collect(Collectors.toList()).forEach(System.out::println);
		
		
		//sg4.filter(distinctByKey(p->p.getNom())).forEach(System.out::println);
		sg4.close();
		
		//exo5 Donnez le nombre de gagnants diff�rents. 
		// Vous utiliserez count() 
		Stream<Gagnants> sg5 = gagnantsTDF.stream();
		int nbGD = (int) sg5.filter(distinctByKey(p->p.getNom())).count();
		System.out.println(nbGD);
		sg5.close();
		
		//exo6 Vous listerez les noms de tous les gagnants, en sautant les deux premiers. Vous utiliserez skip(2) 
		Stream<Gagnants> sg6 = gagnantsTDF.stream();
		sg6.filter(distinctByKey(p->p.getNom())).skip(2).forEach(System.out::println);
		sg6.close();
		
		//exo7 Vous afficherez l�ann�e suivie du nom du gagnant. 
		// Vous utiliserez en tout stream, map et collect. 
		Stream<Gagnants> sg7 = gagnantsTDF.stream();
		sg7.map(x->x.getAnnee() + " " + x.getNom()).forEach(System.out::println);
		sg7.close();
		
		//exo8 Vous afficherez le nombre de caract�res du nom de chaque gagnant sans filtre 
		// Vous utiliserez deux fois map. 
		Stream<Gagnants> sg8 = gagnantsTDF.stream();
		sg8.map(x->x.getNom()).map(x->x.length()).forEach(System.out::println);
		sg8.close();
		
		//exo9 Vous r�cup�rerez les gagnants ayant � Wiggins � dans leur nom. 
		// Vous utiliserez stream, filter contenant une expression lambda, contains, findAny, et vous r�cup�rerez le r�sultat dans un type Optional<Gagnants>.  
		Stream<Gagnants> sg9 = gagnantsTDF.stream();
		sg9.map(x -> x.getNom()).filter(x->x.contains("Wiggins")).forEach(System.out::println);
		sg9.close();
		
		//exo10 Vous identifierez s�il y a eu un gagnant l�ann�e 2014. 
//		Vous utiliserez stream, map, une r�f�rence de m�thode sur gerAnnee(), 
//		filter, findFirst(). Vous r�cup�rerez le r�sultat dans un type Optional<Integer>, 
//		et testerez si le r�sultat est vide avec isPresent() sur Optional. 
		Stream<Gagnants> sg10 = gagnantsTDF.stream();
		sg10.map(x -> x.getAnnee()).filter(x->x==2014).forEach(System.out::println);
		sg10.close();
		
//		Exercice 11 
//		Vous r�cup�rerez le total des kilom�tres effectu�s par l�ensemble des gagnants. 
//		Vous utiliserez stream, map, reduce(0,Integer ::sum) ; vous r�cup�rerez le r�sultat dans un Integer. 
	
//		Exercice 12 
//		Vous r�cup�rez la plus courte distance effectu�e par tous les gagnants, quelle que soit l�ann�e. 
//		Vous utiliserez stream, map, reduce(Integer ::min), vous r�cup�rerez dans un Optional<Integer> 
		 
//		Exercice 13 
//		M�me question que l�exercice pr�c�dent, mais en extrayant la plus longue distance. Vous utiliserez Integer::min 
		 
//		Exercice 14 
//		Vous r�cup�rerez le gagnant qui a eu la vitesse moyenne la plus rapide. Vous avez pour cela une m�thode toute pr�te, getVitesseMoyenne(). Vous utiliserez pour cela :  
//		stream, min(Comparator.comparingDoucle(Gagnants ::getVitesseMoyenne)).  Vous r�cup�rerez dans un Optional<Gagnants> 
		 
//		Exercice 15 
//		Vous ferez la m�me chose, mais avec un code plus resserr�, et en utilisant : Stream, mapToDouble(Gagnants ::getVitesseMoyenne).min() 
		 
//		Exercice 16 
//		Vous grouperez les gagnants par Nom ; en fait seul Froome aura plusieurs entr�es. L�objectif est d�avoir ce genre de sortie : 
//		...,Chris Froome=[Chris Froome, Chris Froome,Chris Froome],... 
//		Vous utiliserez  stream,collect(groupingBy(Gagnants ::getNom)). 
//		Vous r�cup�rerez la liste dans  Map<String,List<Gagnants>> 
		 
//		Exercice 17 
//		Affichez les gagnants par nationalit�. Vous utiliserez : 
//		stream, collect(groupingBy( Gagnants ::getNationalite,counting()) 
		
	}

	public double getVitesseMoyenne() {
		return (getKilometres() / (getTemps().getSeconds() / 3600));
	}

	public int getAnnee() {
		return annnee;
	}

	public void setAnnee(int year) {
		this.annnee = year;
	}

	public String getNationalite() {
		return nationalite;
	}

	public void setNationalite(String nationality) {
		this.nationalite = nationality;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String name) {
		this.nom = name;
	}

	public String getEquipe() {
		return equipe;
	}

	public void setEquipe(String team) {
		this.equipe = team;
	}

	public int getKilometres() {
		return kilometres;
	}

	public void setKilometres(int lengthKm) {
		this.kilometres = lengthKm;
	}

	public Duration getTemps() {
		return temps;
	}

	public void setTemps(Duration winningTime) {
		this.temps = winningTime;
	}

	public int getJoursenjaune() {
		return joursenjaune;
	}

	public void setJoursenjaune(int daysInYellow) {   
		this.joursenjaune = daysInYellow;
	}

	@Override
	public String toString() {
		return nom;
	}
	
	public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) 
	{
	    Map<Object, Boolean> map = new ConcurrentHashMap<>();
	    return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}
	
}
