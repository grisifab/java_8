package java_8_tp9;

import java.util.Arrays;
import java.util.List;

public class Dummy {
	
	private static Integer maStatic;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> listInt = Arrays.asList(12,28,4,13,56);
		int maVariable = 3;
		
		listInt.forEach((el)->{
		maStatic = 12;
		// maVariable = 12; impossible
		// int maVariable = 3; impossible car d�clar� avant
		Integer i = maVariable; // OK car d�clar� dans le main mais apr�s
		System.out.println(el);
		}); 
		
		Integer i = 12;
	}

}
