package tpref;

import java.util.function.BinaryOperator;

public class AppCalc {

	public static Integer applyOperation(BinaryOperator<Integer> bo, Integer a, Integer b) {
		return bo.apply(a,b);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		{	// par m�thode statique
			System.out.println(applyOperation(Calculatrice::add,5,3));
			System.out.println(applyOperation(Calculatrice::sous,5,3));
			System.out.println(applyOperation(Calculatrice::mul,5,3));
			System.out.println(applyOperation(Calculatrice::div,5,3));
		}
		{	// par instance
			Calculatrice calc = new Calculatrice();
			System.out.println(applyOperation(calc::add2,5,3));
			System.out.println(applyOperation(calc::sous2,5,3));
			System.out.println(applyOperation(calc::mul2,5,3));
			System.out.println(applyOperation(calc::div2,5,3));
		}
	}

}
