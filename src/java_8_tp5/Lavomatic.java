package java_8_tp5;

public class Lavomatic {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ImachineAlaver m1 = str->("elle demarre " + str);	
		System.out.println(m1.demarrer(" tr�s tr�s bien"));
		
		// compatible java7 par classe anonyme, dans ce cas on pourrait faire plusieurs m�thodes.
		ImachineAlaver m2 = new ImachineAlaver() {
			@Override
			public String demarrer(String m){
				return ("elle demarre" + m);
			}		
		};
		System.out.println(m2.demarrer(" tr�s tr�s bien"));
	
	}

}
