package thread;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class App {

	
	public static class MyThread extends Thread{
		public void run() {
			System.out.println("ok 1");
			System.out.println(Thread.currentThread().getName());
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyThread th = new App.MyThread();
		th.start();
		
		Thread th1 = new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println(Thread.currentThread().getName());
			}
		});
		th1.start();
		
		for (int i = 0; i < 100; i++) {
			System.out.println(i);
		}

		System.out.println(Thread.currentThread().getName());
		
		ExecutorService es = Executors.newFixedThreadPool(5);
		
		//----------------------
		
		/*
		Future<Integer> ret = es.submit(new Callable<Integer>() {

			@Override
			public Integer call() throws Exception {
				
				Integer resultat = 0;
				for (int i = 0; i < 10; i++) {
					resultat += i;
				}
				
				//boolean ret = false;
				//while(!ret); //Danger !!!
				
				return resultat;
			}
			
		});
		*/
		Future<Integer> ret = es.submit(() -> {				
			Integer resultat = 0;
			for (int i = 0; i < 10; i++) {
				resultat += i;
			}
			return resultat;
		});
		
		Integer resultat = null;
		try {
			resultat = ret.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Resultat de la Task : " + resultat);
	}		
}


