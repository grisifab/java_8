package java_8_tp7;

import java.util.Arrays;
import java.util.List;


public class Dummy {

	public Dummy()  {
		System.out.println("popopopopopopo12 !");
	}
	
	public static void maMethode()  {
		System.out.println("popopopopopopo !");
	}
	
	public void maMethodeInstance()  {
		System.out.println("popopopopopopo2 !");
	}
	
	public static void main(String[] args) {
		
        Integer a[] = new Integer[] { 110, 20, 310, 40 }; 
        List<Integer> list = Arrays.asList(a);
        list.forEach((el)->System.out.println(el));    
        
        // ou
        List<Integer> list2 = Arrays.asList(12,28,4,13,56);
        list2.forEach(System.out::println);
        
        // methode statique chez Dummy donc sans instance (mais non static cot� interface !)
        ImethodReference imr = Dummy::maMethode;
        imr.affiche();
        
        // par lambda
        ImethodReference imr2 = ()-> {System.out.println("new message");};
        imr2.affiche();
        
        // par instance car m�thode non statique
        Dummy dum = new Dummy();
        System.out.println("suis l�");
        ImethodReference imr3 = dum::maMethodeInstance;
        imr3.affiche();
        
        ImethodReference imr4 = Dummy::new;// ref vers public void Dummy() sans cr�er d'objet !
        imr4.affiche();
        
	}

	

}
