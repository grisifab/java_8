package java_8_tp3;

import java.util.function.*;

public class Lavomatic {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ImachineAlaver m1;
		m1 = (str)->{return("elle demarre " + str);};
		System.out.println(m1.demarrer(" tr�s bien"));
		
		// autre m�thode
		Function<String,String> f1 = (String str)->{return("elle demarre " + str);};
		System.out.println(f1.apply("tr�s bien !"));
	}

}
